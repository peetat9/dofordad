var mongo = require('mongodb').MongoClient
var url = 'mongodb://localhost:27017/userDB'
var app = require('express')()
var bodyParser = require('body-parser')
var port = 9876
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json({type: '*/*'}))

app.get("/",function(req,res){
	res.send("<h1>Hello Node.js</h1>")
})

app.get("/search",function(req,res){
	var a = {}
	console.log(req.query)
	console.log(typeof req.query)
	mongo.connect(url, function(err, db) {
		db.collection('dofordad').find(req.query).toArray((err,data)=>{
			if(err) throw err
			delete data['_id']
			console.log(data)
			res.send(data[0])
			db.close()
		})
	})
})


app.listen(port,function(){
	console.log("Starting node.js on port "+port)
})